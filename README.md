XSLT (1.0) Stylesheets for DDI Profiles

DDI31GenerateProfile.xslt and DDI32GenerateProfile.xslt create a DDI Profile
template on the basis of an existing DDI instance.

DDI31Profile2html.xslt and DDI32Profile2html.xslt render a DDI Profile in HTML.


joachim.wackerow@posteo.de